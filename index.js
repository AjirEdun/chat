var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
  socket.on('chat message', function(msg){
    io.emit('chat message', msg);
  });
  
  socket.on('register', function(msg){
	var toSend = JSON.stringify({ name: "Jarvis", color: "black", message: "Let's welcome " + msg + " to Paradise" });
    io.emit('chat message', toSend);
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});